# uxtml-packages

you can download packages as 7zips here

to install them, open the pkg directory
and make a new folder with the same name
of the package.

for example, a folder called:
supertuxkart
Super Tux Kart
supertuxkart1.0
supertuxkart-1-0
etc.

once you boot up uxtml again, rebuilt the
Launchy catalog and look for the package
name.

for example, search:
supertuxkart